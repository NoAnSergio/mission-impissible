﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fallingobj2 : MonoBehaviour
{

    private float timer = 0;
    public Rigidbody2D rb2d = null;
    public float fallSpeed =2f;
    public bool gotaact = false;
    public float Maxfallspeed = 2f;

    Vector3 originalPos;


    // Use this for initialization
    void Start()
    {


        //timer = 5f;

        originalPos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject Player = GameObject.Find("Player");
        MyController playerScript = Player.GetComponent<MyController>();

        transform.Translate(Vector3.down * fallSpeed * Time.deltaTime, Space.World);
       


        Debug.Log(fallSpeed);
        if (timer > 0)
        {
            timer -= Time.fixedDeltaTime;
        }
        else
        {
            timer = 5f;
            //transform.position = originalPos;
            gotaact = false;

        }



    }

    void OnCollisionEnter2D(Collision2D coll)
    {

        if (coll.gameObject.tag == "Ground")
        {

            GetComponent<SpriteRenderer>().color = new Color(0, 1, 0, 1);
            transform.position = originalPos;
            //GameObject.FindWithTag("wet").transform.localScale = new Vector3(0, 0, 0);
            
        }
        if (!gotaact)
        {
            if (coll.gameObject.tag == "Jugador")
            {
                GameObject Player = GameObject.Find("Player");
                MyController playerScript = Player.GetComponent<MyController>();

                playerScript.GetComponent<SpriteRenderer>().color = new Color(0, 1, 1, 1);
                transform.position = originalPos;

            }
        }


    }
}