﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public class MyController : MonoBehaviour {
//    //Privadas
//    private Rigidbody2D rbd2 = null;
//    private Animator anim;
//    private float move = 0f;
//    private bool flipped = false;
//    private bool muerto = false;

//    //Publicas
//    public float maxS = 11f;
//    void Awake () {
//        rbd2 = GetComponent<Rigidbody2D>();
//        anim = GetComponent<Animator>();
//	}
	
//    void FixedUpdate()
//    {
//        move = Input.GetAxis("Horizontal");
        
//        if (muerto == false)
//        {
//            rbd2.velocity = new Vector2(/*rbd2.velocity.x+*/(move * maxS), rbd2.velocity.y);
//        }

//        if (rbd2.velocity.x>0.001f|| rbd2.velocity.x < -0.001f){
//            if ((rbd2.velocity.x < -0.001&&!flipped) ||(rbd2.velocity.x > -0.001f && flipped)){
//                flipped = !flipped;
//                this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
//            }
//            anim.SetBool("Walking", true);
//        }else{
//            anim.SetBool("Walking", false);
//        }
//    }
//    void OnCollisionEnter2D(Collision2D coll)
//    {
//        if (coll.gameObject.tag == "Muerte")
//        {
//            muerto = true;
//            anim.SetBool("Dying", true);
//        }
//    }
//}

public class MyController : MonoBehaviour
{
   public Rigidbody2D rb2d = null;
    public float move = 0f;
    public float maxS = 11f;
    private bool isJumping = false;
    public float jumpHeight= 0f;
    public bool TrapActivate = false;
   

   

    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (!TrapActivate)
        {
            move = Input.GetAxis("Horizontal");
           
                rb2d.velocity = new Vector2(/*rbd2.velocity.x+*/(move * maxS), rb2d.velocity.y);
            

            if (Input.GetKeyDown(KeyCode.Space) && !isJumping)
            {
                rb2d.AddForce(new Vector3(0, 5, 0), ForceMode2D.Impulse);
                isJumping = true;
            }
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Ground")
        {
            isJumping = false;
        }
       
    }
    

}
