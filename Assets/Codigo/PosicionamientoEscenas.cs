﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PosicionamientoEscenas : MonoBehaviour {

    // Use this for initialization

    string [] floorOne = new string[] {"Piso1Matetaticas", "Piso1Lieteratura", "Piso1Geografia", "Piso1Musica" };
    
    void Createroom(string roomName)
    {
        SceneManager.LoadScene(roomName, LoadSceneMode.Additive);
        
    }
    void LoadFloorOne()
    {
        
        int num = Random.Range(0, 3);
        string room = floorOne[num];
        Createroom(room);
    }
   
    void Awake () {
        LoadFloorOne();

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
