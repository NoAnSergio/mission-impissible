﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slip : MonoBehaviour
{

    public float slipvel = 9f;
    private float timer = 0f;
   



    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(timer);
        GameObject Player = GameObject.Find("Player");
        MyController playerScript = Player.GetComponent<MyController>();

        if (timer > 0)
        {
            timer -= Time.fixedDeltaTime;
            if (timer < 1.5)
            {
                playerScript.rb2d.velocity = Vector3.zero;
            }

        }
      
        else
        {
            timer = 0;
            playerScript.TrapActivate = false;
        }

    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        GameObject Player = GameObject.Find("Player");
        MyController playerScript = Player.GetComponent<MyController>();

        if (coll.gameObject.tag == "Jugador")
        {

            GetComponent<SpriteRenderer>().color = new Color(0, 1, 0, 1);
            playerScript.TrapActivate = true;
            timer = 2.5f;
           

            
            
        }



    }
  

}