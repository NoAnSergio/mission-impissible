﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public Transform Player;

    void Update()
    {

        if (Input.GetAxis("Horizontal") > 0)
        {

            transform.position = new Vector3(Player.position.x + 4, Player.position.y+2, transform.position.z);

        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            transform.position = new Vector3(Player.position.x - 4, transform.position.y, transform.position.z);
        }
        else
        {
            transform.position = new Vector3(Player.position.x, transform.position.y, transform.position.z);
        }

    }
}
